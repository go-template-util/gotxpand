package goterrors

//
// fmt.Errorf("File not received completely: %w", ErrFileNotFound)
type Error string

func (e Error) Error() string {
	return string(e)
}

const (
	ErrDataFormat = Error("Data Format Error")
	ErrFileLoad   = Error("File Load Error")
)
