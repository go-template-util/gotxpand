package got

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSplitKeyOneSegment(t *testing.T) {
	data := NewData()
	ns, n := data.splitKey("root")
	assert.Equal(t, "", ns)
	assert.Equal(t, "root", n)
}

func TestSplitKeyTwoSegment(t *testing.T) {
	data := NewData()
	ns, n := data.splitKey("root.subkey")
	assert.Equal(t, "root", ns)
	assert.Equal(t, "subkey", n)
}

func TestAddKey(t *testing.T) {
	data := NewData()

	// root should not exist yet
	k, ok := data.Values["root"]
	assert.False(t, ok)

	// Add key root/subkey
	data.addKey("root.subkey")

	k, ok = data.Values["root"]
	assert.True(t, ok)

	k, ok = k.(map[string]interface{})["subkey"]
	assert.True(t, ok)
}

func TestAddKeyOnIncorrectDataType(t *testing.T) {
	data := NewData()

	// 'root' is a literal, not a dictionary
	data.Values["root"] = "rootVal"

	// Trying to add a subkey to 'root' should fail
	_, err := data.addKey("root.subkey")
	assert.EqualError(t, err, "Unexpected data type for key 'root': Data Format Error")
}

func TestMergeWithKey(t *testing.T) {
	data := NewData()

	newData := map[string]interface{}{
		"key1": "val1",
		"key2": "val2",
	}

	err := data.Merge("root", newData)
	assert.NoError(t, err)

	k, ok := data.Values["root"]
	assert.True(t, ok)

	rootMap := k.(map[string]interface{})
	v, _ := rootMap["key1"]
	assert.Equal(t, v, "val1")

	v, _ = rootMap["key2"]
	assert.Equal(t, v, "val2")
}

func TestMergeWithMultiSegKey(t *testing.T) {
	data := NewData()

	newData := map[string]interface{}{
		"key1": "val1",
		"key2": "val2",
	}

	err := data.Merge("root.subkey", newData)
	assert.NoError(t, err)

	k, ok := data.Values["root"]
	assert.True(t, ok)

	rootMap := k.(map[string]interface{})
	k, _ = rootMap["subkey"]

	subMap := k.(map[string]interface{})

	v, _ := subMap["key1"]
	assert.Equal(t, v, "val1")

	v, _ = subMap["key2"]
	assert.Equal(t, v, "val2")
}

func TestSetString(t *testing.T) {
	data := NewData()

	err := data.SetString("root.subkey", "val")
	assert.NoError(t, err)

	k, ok := data.Values["root"]
	assert.True(t, ok)

	rootMap := k.(map[string]interface{})
	v, _ := rootMap["subkey"]
	assert.Equal(t, v, "val")

	err = data.SetString("root.subkey.a", "aval")
	assert.EqualError(t, err, "Unexpected data type for key 'root.subkey': Data Format Error")
}
