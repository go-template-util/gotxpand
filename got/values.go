package got

import (
	"fmt"
	"gitlab.com/go-template-util/gotxpand/goterrors"
	"strings"
)

type Data struct {
	Values map[string]interface{}
}

// NewData creates Data object
func NewData() *Data {
	return &Data{
		Values: make(map[string]interface{}),
	}
}

// SetString sets the value for a key
//
// key can be a string in the format "namespace.name", where "namespace" can be seen as
// the directory in which the "name" exists
//
// "namespace" can be a dot separated list of segments, where each segment denotes
// a sub name-space
func (d *Data) SetString(key string, val string) error {
	valMap := d.Values

	ns, n := d.splitKey(key)

	if len(ns) > 0 {
		var err error
		valMap, err = d.addKey(ns)
		if err != nil {
			return err
		}
	}

	valMap[n] = val
	return nil
}

// Merge merges the given map 'newVal' to the values at key 'key'
func (d *Data) Merge(key string, newVal map[string]interface{}) error {
	// If no key is specified, merge it to the root
	if len(key) == 0 {
		return mergeMaps(d.Values, newVal)
	}

	// Insert if a new key, get if existing
	subMap, err := d.addKey(key)
	if err != nil {
		return err
	}

	return mergeMaps(subMap, newVal)
}

// splitKey splits a key in the format namespace.name into two
// namespace itself can be of multiple segments separated by '.'
func (d *Data) splitKey(key string) (namespace, name string) {
	switch i := strings.LastIndex(key, "."); i {
	case -1:
		return "", key

	default:
		return key[:i], key[i+1:]
	}
}

// addKey inserts the key into the Values map
// If the key alread exists (or a part of it), it will use the existing map
func (d *Data) addKey(key string) (map[string]interface{}, error) {
	// Split the key into multiple segments using '.' as delimiter
	segList := strings.Split(key, ".")

	dict := d.Values
	for i, seg := range segList {
		// If the segment already exists, use it
		// If not, create a new sub-map
		if v, exists := dict[seg]; exists {
			switch subObj := v.(type) {
			case map[string]interface{}:
				dict = subObj
			default:
				return nil, fmt.Errorf("Unexpected data type for key '%s': %w", strings.Join(segList[:i+1], "."), goterrors.ErrDataFormat)
			}
		} else {
			newDict := make(map[string]interface{})
			dict[seg] = newDict
			dict = newDict
		}
	}
	return dict, nil
}

// mergeMaps deep merges the map 'src' into 'dst'
// this is a recursive function
func mergeMaps(dst, src map[string]interface{}) error {
	// Get each key value from 'src' to add it to 'dst'
	for srcKey, srcVal := range src {
		// If the key already exists in dst, merge with it
		dstVal, exists := dst[srcKey]
		if exists {
			// The value in dst can be a map, list, or a literal
			// If it is a map, recursively merge the sub-maps
			// If it is a list, copy the contents from src into dst
			// If it is literal, just overwrite the value
			switch dstVal := dstVal.(type) {
			// map
			case map[string]interface{}:
				switch srcMap := srcVal.(type) {
				case map[string]interface{}:
					if err := mergeMaps(dstVal, srcMap); err != nil {
						return err
					}
				default:
					return fmt.Errorf("Type mismatch")
				}
			// list
			case []interface{}:
				switch srcList := srcVal.(type) {
				case []interface{}:
					for _, srcVal := range srcList {
						dstVal = append(dstVal, srcVal)
					}
					dst[srcKey] = dstVal
				default:
					return fmt.Errorf("Type mismatch")
				}
			// literal
			default:
				dst[srcKey] = srcVal
			}
		} else {
			// new key, just set it
			dst[srcKey] = srcVal
		}
	}
	return nil
}
