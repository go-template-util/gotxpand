package got

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestExpand(t *testing.T) {
	data := NewData()
	data.Values["key1"] = "val1"
	data.Values["key2"] = "val2"

	tData := []byte("{| .key1 |}")
	o, err := Expand(tData, data)
	assert.Equal(t, "val1", string(o))
	assert.NoError(t, err)
}
