package got

import (
	"bytes"
	"text/template"
)

// Delimiters
const (
	DefStartDelim = "{|"
	DefEndDelim   = "|}"
)

var (
	startDelim = DefStartDelim
	endDelim   = DefEndDelim
)

// SetDelim configures the delimiters
func SetDelim(s, e string) {
	startDelim, endDelim = s, e
}

// Expand expands the template `tContent` using the values provided in `data`
// Expanded content is returned
func Expand(tContent []byte, data *Data) ([]byte, error) {
	t, err := template.New("xpand").
		Delims(startDelim, endDelim).
		Parse(string(tContent))
	if err != nil {
		return nil, err
	}

	var b []byte
	w := bytes.NewBuffer(b)
	if err := t.Execute(w, data.Values); err != nil {
		return nil, err
	}

	return w.Bytes(), nil
}
