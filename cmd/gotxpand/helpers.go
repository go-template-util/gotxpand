package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"github.com/ghodss/yaml"

	"gitlab.com/go-template-util/gotxpand/got"
	"gitlab.com/go-template-util/gotxpand/goterrors"
)

// loadYAML loads a YAML file
func loadYAML(fileName string, obj interface{}) error {
	content, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Print("Failed to read file:", fileName, ". err=", err)
		return err
	}

	err = yaml.Unmarshal(content, obj)

	if err != nil {
		log.Print("Failed to load YAML:", fileName, ". err=", err)
	}

	return err
}

// loadValueFiles loads the data from value-files into data
// Command line argument for value files can be specified as:
// 		--file val-file.yaml
//		--file root.subkey=val-file.yaml - loads the valuefile under subkey root.subkey
func loadValueFiles(fileArgs []string, data *got.Data) error {
	for _, argVal := range fileArgs {
		vals := strings.SplitN(argVal, "=", 2)
		var fileKey, fileName string

		switch len(vals) {
		case 1:
			fileKey, fileName = "", vals[0]

		case 2:
			fileKey, fileName = vals[0], vals[1]

		default:
			return fmt.Errorf("Expected format [key=]val for file name %s: %w", argVal, goterrors.ErrDataFormat)
		}

		newVal := make(map[string]interface{})
		if err := loadYAML(fileName, &newVal); err != nil {
			return fmt.Errorf("Failed to load %s: %w", fileName, goterrors.ErrFileLoad)
		}

		data.Merge(fileKey, newVal)
	}

	return nil
}

// loadOverrideValues loads the values provided in the command line to data
// values can be provided in commandline with --set option as:
//		--set root.subkey=value
//		--set key=value
func loadOverrideValues(valArgs []string, data *got.Data) error {
	for _, argVal := range valArgs {
		vals := strings.SplitN(argVal, "=", 2)
		var key, value string

		switch len(vals) {
		case 2:
			key, value = vals[0], vals[1]

		default:
			return fmt.Errorf("Expected format [key=]val for '%s': %w", argVal, goterrors.ErrDataFormat)
		}

		// If value is empty, ignore it.
		// This will allow the user to try to override using environment variables, if set
		if len(value) > 0 {
			data.SetString(key, value)
		}
	}
	return nil
}

const (
	errMissingArguments = goterrors.Error("Missing Arguments")
)

// validateArgs verifies if the required arguments are provided
func validateArgs() error {
	errCount := 0
	if len(templateFileArg) == 0 {
		errCount++
		log.Print("Missing 'template' argument")
	}
	if errCount > 0 {
		return errMissingArguments
	}
	return nil
}
