package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/go-template-util/gotxpand/got"
)

var (
	valuesArg       MultiValueArg
	valFilesArg     MultiValueArg
	templateFileArg string
	outFileArg      string
	startDelim      string
	endDelim        string
)

func init() {
	flag.Var(&valuesArg, "set", "Add a value key=value")
	flag.Var(&valFilesArg, "data-file", "YAML file with values")
	flag.StringVar(&templateFileArg, "template", "", "Template file name")
	flag.StringVar(&outFileArg, "out", "", "Output file name")
	flag.StringVar(&startDelim, "start-delim", got.DefStartDelim, "Start Delimiter for template variables")
	flag.StringVar(&endDelim, "end-delim", got.DefEndDelim, "End Delimiter for template variables")
}

func main() {
	flag.Parse()

	if err := validateArgs(); err != nil {
		log.Print("ERROR: ", err)
		os.Exit(1)
	}

	data := got.NewData()

	// Load values from files
	if err := loadValueFiles(valFilesArg.ValList, data); err != nil {
		log.Print("ERROR: ", err)
		os.Exit(1)
	}

	// Load values from command-line arguments
	if err := loadOverrideValues(valuesArg.ValList, data); err != nil {
		log.Print("ERROR: ", err)
		os.Exit(1)
	}

	// Load the template
	log.Print("Loading template ", templateFileArg)
	template, err := ioutil.ReadFile(templateFileArg)
	if err != nil {
		log.Print("Failed to read file:", templateFileArg, ". err=", err)
		os.Exit(1)
	}

	// Execute the template
	expanded, err := got.Expand(template, data)
	if err != nil {
		log.Print(err)
		os.Exit(1)
	}

	// If an output file is not specified, print the output on console
	// If a file is specified for output, write to it
	if len(outFileArg) == 0 {
		fmt.Printf("%s\n", expanded)
	} else {
		if err := ioutil.WriteFile(outFileArg, expanded, 0644); err != nil {
			log.Print(err)
			os.Exit(1)
		}

	}
}
