package main

import (
	"fmt"
	"strings"
)

type KeyVal struct {
	Key string
	Val string
}

// MultiKeyValueArg holds multiple key-value pairs passed in flag
type MultiKeyValueArg struct {
	KVList []KeyVal
}

func (v *MultiKeyValueArg) String() string {
	var b strings.Builder

	count := len(v.KVList)
	for i, kv := range v.KVList {
		b.WriteString(kv.Key)
		b.WriteString("=")
		b.WriteString(kv.Val)
		if i < count-1 {
			b.WriteString(", ")
		}
	}
	return b.String()
}

func (v *MultiKeyValueArg) Set(arg string) error {
	val := strings.SplitN(arg, "=", 2)
	if len(val) != 2 {
		return fmt.Errorf("Expected format key=val")
	}

	kv := KeyVal{
		Key: val[0],
		Val: val[1],
	}

	v.KVList = append(v.KVList, kv)

	return nil
}

// MultiValueArg holds multiple key-value pairs passed in flag
type MultiValueArg struct {
	ValList []string
}

func (a *MultiValueArg) String() string {
	var b strings.Builder

	count := len(a.ValList)
	for i, v := range a.ValList {
		b.WriteString(v)
		if i < count-1 {
			b.WriteString(", ")
		}
	}
	return b.String()
}

func (a *MultiValueArg) Set(arg string) error {
	a.ValList = append(a.ValList, arg)

	return nil
}
