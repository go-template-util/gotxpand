# Overview
A simple utility to process go templates from command line.  It takes a template file, data required to expand the template file, and gives the processed result.  Data for template can be taken from YAML files and command-line arguments.  Values provided in command-line overrides the values taken from YAML files.

## Example:

template.got:

  ```shell
  This is a sample template file

  key1={| .key1 |}
  key2={| .key2 |}
  key3.subkey1={| .key3.subkey1 |}
  key3.subkey2={| .key3.subkey2 |}
  ```

v1.yaml:
  ```yaml
  key1: v1-value1
  key2: v1-value2
  key3:
    subkey1: v1-subval1
  ```

v2.yaml:

  ```yaml
  key2: v2-value2
  key3:
    subkey1: v2-subval1
    subkey2: v2-subval2
  ```

Run the command with:

  ```shell
  gotxpand --template=template.got \    # Template argument
      --data-file=v1.yaml \                 # First value file
      --data-file=v2.yaml  \                # Second value file
      --set key3.subkey1=cmdline-val   # Set/Override from command line
  ```

The template template.got will be expanded as:
  ```
  This is a sample template file

  key1=v1-value1
  key2=v2-value2
  key3.subkey1=cmdline-val
  key3.subkey2=v2-subval2
  ```

# Usage
## Installation
  ```shell
  go get -u gitlab.com/go-template-util/gotxpand/cmd/gotxpand
  ```

## Options
### template
Used to specify the template file name

### file
Used to specify a value file.  `--data-file` option can be repeated to specify multiple data files.  If there are multiple files specified, values from the second one will be merged with the first.  In case of common keys, second one will override the first. 

### set
Set a value explicitly from command-line.
Set can be used multiple times to set multiple values.  It overrides the settings from data-files

For cases like overriding using environment variable, if set, set can be used like:
  ```shell
  gotxpand \
      --set name=default-val \    # Set the default value for "name"
      --set name=$NAME_ENV        # If there is an environment variable NAME_ENV defined, use it
  ```
### out
Specify the output file name.  If not specified, result will be printed on console
