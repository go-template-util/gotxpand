module gitlab.com/go-template-util/gotxpand

go 1.13

require (
	github.com/ghodss/yaml v1.0.0
	github.com/pkg/browser v0.0.0-20180916011732-0a3d74bf9ce4 // indirect
	github.com/stretchr/testify v1.5.1
	gitlab.com/golang-commonmark/linkify v0.0.0-20200225224916-64bca66f6ad3 // indirect
	gitlab.com/golang-commonmark/markdown v0.0.0-20191127184510-91b5b3c99c19 // indirect
	gitlab.com/golang-commonmark/mdtool v0.0.0-20180916031940-c661f0ca84f4 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
